
const Binance = require('node-binance-api');
const rimraf = require('rimraf');
import * as moment from 'moment';
import * as fs from 'fs';


export class Index {
  io;
  startTime: any;
  prevSecond: number = 0;
  unsyncCount: number = 0;

  constructor() {
    this.resetLogs();
    this.initWS();
    this.initBinance();
  }

  initWS(): void {
    const express = require('express');
    const app = express();
    const http = require('http');
    const server = http.createServer(app);
    const { Server } = require("socket.io");
    this.io = new Server(server);

    this.io.on('connection', (socket) => {
      console.log('a user connected');

      socket.on('message', (msg) => {
        console.log('message: ' + msg);
      });

      socket.on('disconnect', () => {
        console.log('user disconnected');
      });
    });

    server.listen(3000, () => {
      console.log(`${new Date().toISOString()}: listening on *:3000`);
    });
  }

  WSEmit(data: any): void {
    this.io.emit('message', JSON.stringify(data));
  }

  initBinance(): void {
    this.startTime = new Date();
    const binance = new Binance();

    binance.futuresMarkPriceStream((pairsArr) => {
      const pair = pairsArr[0];
      if (pair?.eventTime) {
        this.logToFile(`./logs/`, 'time.log', pair.eventTime);

        const pairs = [];
        for (const pair of pairsArr) {
          const symbol = pair.symbol;
          const price = Number(pair.markPrice);
          if (symbol && price) {
            pairs.push([symbol, price]);
          }
        }

        this.logDataToPath(`./logs/`, 'prices.log', pairs);

        this.sendData();
      }
    });
  }

  logToFile(path: string, fileName: string, data: any): void {
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path, { recursive: true });
    }

    fs.appendFileSync(`${path}${fileName}`, data + ',');
  }

  logDataToPath(dirPath: string, fileName: string, data: any): void {
    if (!fs.existsSync(dirPath)) {
      fs.mkdirSync(dirPath, { recursive: true });
    }

    fs.appendFileSync(`${dirPath}${fileName}`, JSON.stringify(data) + '\n');
  }

  getTimeDifference(startTs: Date, endTs: Date): string {
    const startTime = moment(startTs);
    const endTime = moment(endTs);

    const duration = moment.duration(endTime.diff(startTime));
    const d = duration.days();
    const h = duration.hours();
    const m = duration.minutes();
    const s = duration.seconds();

    return `${d}D${h}:${m}:${s}`;
  }

  sendData(): void {
    const workingTime = this.getTimeDifference(this.startTime, new Date());

    console.log(workingTime);

    this.WSEmit({ type: 'info', data: workingTime });
  }

  resetLogs(): void {
    const path = './logs/';

    if (fs.existsSync(path)) {
      rimraf(path, () => { });
    }
  }
}


new Index();


